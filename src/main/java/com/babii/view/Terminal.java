package com.babii.view;

import com.babii.controller.Airline;
import com.babii.model.Plane;
import com.babii.utility.Creator;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Terminal {
    private static Scanner scanner = new Scanner(System.in);
    private static List<Plane> planes = Creator.createPlanes();
    private static Airline airline = new Airline(planes);

    private static void showStartMessage() {
        System.out.println("Press 1 to show all planes");
        System.out.println("Press 2 to get the sum of passengers capacity");
        System.out.println("Press 3 to get the sum of load capacity");
        System.out.println("Press 4 to sort planes in the company by the flight range");
        System.out.println("Press 5 to find a planes in the company "
                + "that corresponds to a given range of parameters for fuel consumption");
        System.out.println("Press 0 to exit");
    }

    public static void startTerminal() {
        while (true) {
            showStartMessage();
            String terminalNumber = scanner.next();
            if (terminalNumber.equals("1")) {
                viewPlanes();
            } else if (terminalNumber.equals("2")) {
                viewSumOfPassengerCapacity();
            } else if (terminalNumber.equals("3")) {
                viewSumOfLoadCapacity();
            } else if (terminalNumber.equals("4")) {
                viewSortedByFlightRange();
            } else if (terminalNumber.equals("5")) {
                viewByFuelConsumption();
            } else if (terminalNumber.equals("0")) {
                System.exit(0);
            } else {
                System.out.println("You pressed an illegal number, try again");
            }
            System.out.println("\n");
        }
    }

    private static void viewPlanes() {
        planes.forEach(System.out::println);
    }

    private static void viewSumOfPassengerCapacity() {
        System.out.println("The sum of passenger capacity is "
                + airline.getSumOfPassengerCapacity());
    }

    private static void viewSumOfLoadCapacity() {
        System.out.println("The sum of load capacity is "
                + airline.getSumOfLoadCapacity());
    }

    private static void viewSortedByFlightRange() {
        airline.sortByFlightRange().forEach(System.out::println);
    }

    private static void viewByFuelConsumption() {
        int beginningOfInterval;
        int endOfInterval;
        while (true) {
            try {
                System.out.println("Enter the beginning of the interval:");
                beginningOfInterval = scanner.nextInt();
                System.out.println("Enter the end of the interval:");
                endOfInterval = scanner.nextInt();
                if (beginningOfInterval >= endOfInterval || beginningOfInterval <= 0
                        || endOfInterval <= 0) {
                    throw new InputMismatchException();
                }
                break;
            } catch (InputMismatchException e) {
                System.out.println("You entered an illegal interval, try again");
                scanner.nextLine();
            }
        }
        airline.getByFuelConsumption(beginningOfInterval, endOfInterval).forEach(System.out::println);
    }
}
