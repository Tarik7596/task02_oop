package com.babii.controller;


import com.babii.model.Plane;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Airline {
    private List<Plane> planes;

    public Airline(List<Plane> planes) {
        this.planes = planes;
    }

    public int getSumOfPassengerCapacity() {
        return planes.stream()
                .mapToInt(Plane::getPassengerCapacity)
                .sum();
    }

    public int getSumOfLoadCapacity() {
        return planes.stream()
                .mapToInt(Plane::getLoadCapacity)
                .sum();
    }

    public List<Plane> sortByFlightRange() {
        Comparator<Plane> cmd = Comparator.comparing(Plane::getFlightRange);
        return planes.stream()
                .sorted(cmd)
                .collect(Collectors.toList());
    }

    public List<Plane> getByFuelConsumption(int from, int to) {
        return planes.stream()
                .filter((p) -> p.getFuelConsumption() >= from &&
                        p.getFuelConsumption() <= to)
                .collect(Collectors.toList());
    }
}
