package com.babii;

import com.babii.view.Terminal;

public class Application {
    public static void main(String[] args) {
        Terminal.startTerminal();
    }
}
