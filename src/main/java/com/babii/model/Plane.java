package com.babii.model;


public abstract class Plane {
    private String name;
    private int flightRange;
    private int fuelConsumption;
    private int passengerCapacity;
    private int loadCapacity;

    public Plane(String name, int flightRange, int fuelConsumption, int passengerCapacity, int loadCapacity) {
        this.name = name;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
        this.passengerCapacity = passengerCapacity;
        this.loadCapacity = loadCapacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }

    public int getLoadCapacity() {
        return loadCapacity;
    }

    public void setLoadCapacity(int loadCapacity) {
        this.loadCapacity = loadCapacity;
    }

    public abstract String takeoff();

    public abstract String fly();

    public abstract String land();

    @Override
    public String toString() {
        return "Plane{" +
                "name='" + name + '\'' +
                ", flightRange=" + flightRange +
                ", fuelConsumption=" + fuelConsumption +
                ", passengerCapacity=" + passengerCapacity +
                ", loadCapacity=" + loadCapacity +
                ", ";
    }
}
