package com.babii.model;


public class CargoPlane extends Plane {
    private int cargoHold;

    public CargoPlane(String name, int flightRange, int fuelConsumption, int passengerCapacity, int loadCapacity, int cargoHold) {
        super(name, flightRange, fuelConsumption, passengerCapacity, loadCapacity);
        this.cargoHold = cargoHold;
    }

    public int getCargoHold() {
        return cargoHold;
    }

    public void setCargoHold(int cargoHold) {
        this.cargoHold = cargoHold;
    }

    public String takeoff() {
        return "Cargo plane takeoffs";
    }

    public String fly() {
        return "Cargo plane flies";
    }

    public String land() {
        return "Cargo plane lands";
    }

    @Override
    public String toString() {
        return super.toString() + "cargoHold="
                + cargoHold +
                '}';
    }
}
