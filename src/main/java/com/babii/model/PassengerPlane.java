package com.babii.model;


public class PassengerPlane extends Plane{
    private int rowOfSeats;

    public PassengerPlane(String name, int flightRange, int fuelConsumption, int passengerCapacity, int loadCapacity, int rowOfSeats) {
        super(name, flightRange, fuelConsumption, passengerCapacity, loadCapacity);
        this.rowOfSeats = rowOfSeats;
    }

    public int getRowOfSeats() {
        return rowOfSeats;
    }

    public void setRowOfSeats(int rowOfSeats) {
        this.rowOfSeats = rowOfSeats;
    }

    public String takeoff() {
        return "Passenger plane takeoffs";
    }

    public String fly() {
        return "Passenger plane flies";
    }

    public String land() {
        return "Passenger plane lands";
    }

    @Override
    public String toString() {
        return super.toString() + "rowOfSeats="
                + rowOfSeats +
                '}';
    }
}
