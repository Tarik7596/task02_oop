package com.babii.utility;

import com.babii.model.CargoPlane;
import com.babii.model.PassengerPlane;
import com.babii.model.Plane;
import java.util.ArrayList;
import java.util.List;

public final class Creator {
    public static List<Plane> createPlanes(){
        List<Plane> planes = new ArrayList<>();
        Plane plane1 = new PassengerPlane("Boeing 737", 5925, 25,
                215, 74389, 2);
        Plane plane2 = new PassengerPlane("Boeing 777", 14075, 12,
                365, 351534, 3);
        Plane plane3 = new CargoPlane("An-225 Mriya", 15400, 16,
                6, 640000, 1300);
        Plane plane4 = new CargoPlane("An-124 Ruslan", 5200, 20,
                88, 405000, 800);
        Plane plane5 = new PassengerPlane("Boeing 747", 14815, 13,
                581, 275600, 4);
        Plane plane6 = new CargoPlane("Airbus A330", 4400, 23,
                5, 348300, 650);
        planes.add(plane1);
        planes.add(plane2);
        planes.add(plane3);
        planes.add(plane4);
        planes.add(plane5);
        planes.add(plane6);
        return planes;
    }
}
